<div id="nid" style="display: none"><?php echo $quiz->nid; ?></div>
<div id="quiztitle" style="display: none"><?php echo check_plain($quiz->title); ?></div>
<!-- Quiz Body/Intro -->
<?php
  global $base_path;
  global $base_url;
$path = $base_url . "/" . drupal_get_path("module", "jquery_quiz");
  // Count the quiz questions.
  $count_q = count($quiz->questions);
  ?>
<div class="qnumFull" style="display:none">
  <!-- Quiz Score -->
  <div class="scored"> <h2>Score: <span class="score">0</span> <span id="ques-count" style="display:none"><?php echo $count_q; ?></span></h2></div>
  <!-- Quiz Counter -->
  <div class="ques"> <h2>Question: <span class="qnum">0</span> of <?php echo $count_q; ?></h2></div>
</div>
<div class="content-wrapper">
<div class='intro'>
  <script> var total_num_questions = <?php echo $count_q; ?>;</script>
  <script> var base_url = "<?php echo $base_url; ?>";</script>
  <?php
  // Quiz intro Image.
  if (isset($quiz->field_primary_image[LANGUAGE_NONE][0]['uri']) && !empty($quiz->field_primary_image[LANGUAGE_NONE][0]['uri'])) {
    $p = theme('image_style', array('style_name' => 'scale_230_120', 'path' => $quiz->field_primary_image[LANGUAGE_NONE][0]['uri']));
    echo "<div class='intro2'>";
    echo '<div class="intro-image">' . $p . '</div>';
    echo '<div class="intro-text">' . $quiz->field_primary_image_credit[LANGUAGE_NONE][0]['value'] . '</div>';
    echo "</div>";
  }
  // Quiz intro.
  echo '<div class="quiz-intro">' . ($quiz->body[LANGUAGE_NONE][0]['safe_value']) . '</div>';
  ?>

  <!-- Quiz Start Button -->
  <div class="start">
    <span class="quiz-trigger" onClick="showNext(1, 0);">Start the Quiz</span> </div>
</div>
<!-- Quiz Body/Intro End -->

 <!-- Place for qnumfull -->


<div class='quiz-questions'>
<?php
// Loop thru the quiz questions.
// Quiz questions dev wrapper.
$qi = 0;
foreach ($quiz->questions AS $question) {
  $qi++;

  //question dev
  echo "<div class='question-$qi' style='display:none;'>";
  // Quiz Question
  echo "<div class='inquestion'>";
  echo "<div class='question-text'>Q: " . $question->field_question[LANGUAGE_NONE][0]['safe_value'] . "</div>";
  //echo "<pre>"; print_r($question);
  // Show the question picture.
  if (isset($question->field_image[LANGUAGE_NONE][0]['uri']) && !empty($question->field_image[LANGUAGE_NONE][0]['uri'])) {
    $p = theme('image_style', array('style_name' => 'scale_230_120', 'path' => $question->field_image[LANGUAGE_NONE][0]['uri']));
    echo '<div class="q-image-wrapper"><div class="q-image">' . $p . '</div>';
    // Show the question picture credits.
if (isset($question->field_primary_image_credit[LANGUAGE_NONE][0]['value']))
    echo "<div class='question-credit'>" . $question->field_primary_image_credit[LANGUAGE_NONE][0]['value'] . "</div>";
    echo "</div>"; //qteaser
  }
  // Question correct answer.
  $correct_value = $question->field_correct_answer_number[LANGUAGE_NONE][0]['value'];
  echo "</div>";
  // Question Explination tip.
  echo "<div class='explaination explaination-$qi' style='display:none'>" . $question->field_answer_explaination[LANGUAGE_NONE][0]['value'] . "</div>";

  // Question Alternatives/Options.
  echo "
      <div class='quiz-alternatives'>
        <ul id='question-options-$qi'>";
  $i = 0;
  foreach ($question->field_question_alternatives[LANGUAGE_NONE] AS $alternative) {
    $i++;
    echo "<li class='alternative' aid='$i' id='$qi-option-$i' onClick='alternativClicked($i, $correct_value, $qi);'>" . $alternative['safe_value'] . "</li>";
  }
  echo "
        </ul>
      </div>";
  //Question Alternatives/Options end.
  // Next Question or Get results Links.
  $next = $qi + 1;
  if ($qi == $count_q) {
    echo "<div class='quiz-trigger quiz-trigger-$qi' style='display:none' onClick='showScore($next, $qi);'>Get Results</div>";
  } else {
    echo "<div class='quiz-trigger quiz-trigger-$qi' style='display:none' onClick='showNext($next, $qi);'>Next Question</div>";
  }
  //question dev end
  echo "</div>";  // question-qi
} // loop thru questions end
// quiz questions dev wrapper end
?>
</div>
<!-- Quiz Final Slide Get Results -->
<div class='get-results' style='display:none'>
  <!-- Quiz Show Scored results -->
 <div class="top-share"> <div class='scored' style='display:block'>You scored <span id="scored">0</span> out of <span id="scored-out"><?php echo $count_q; ?></span></div>
  <!-- Quiz Show Results/Expert Level End-->
  <!-- Social Share-->
  <div class='social-share'>
    <div class="shared"> SHARE YOUR RESULTS </div>
    <div class="f-warp fb-share social-sharing">
    <div class="f-warp fb-share social-sharing r-t float-r">
      <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo rawurlencode($base_url . '/'. drupal_get_path_alias("node/" . $quiz->nid)); ?>">
       <img class="box-inline float-l" src="<?php echo $path; ?>/images/facebook1.png" alt="" title=""/> 
      </a>
      <div class="tw-share">
        <a id="tweet-url" target="new" href=''>
          <img class="box-inline float-l" src="<?php echo $path; ?>/images/twitter.png" alt="" title=""/>
        </a>
      </div>
    </div>
    </div>
  </div>
  </div>

  <!-- Social Share End-->
  
  <!-- Show image -->
  <div class="scorecard-image">
  <?php
  
//  $path = drupal_get_path("module", "jquery_quiz");
  // we will echo all the images hidden and show them via jquery - based on the score
  for ($i = 1; $i <= 4; $i++) {
    
  $file_name = $path . "/images/feedback{$i}.gif";
  echo "<span class='feedback-image-$i' style='display:none;'><img src='" . $file_name . "'></span>";
}
  
  ?>
  </div>
  <!-- End show Image -->
  <div class="expert">
  <!-- Quiz Feedback -->
  <div class='expert-level'>
    <?php
    // we will write down the feedback for all the levels here - display none
    // will show the relevant one in js based on the score level
    for ($i = 1; $i <= 4; $i++) {
      $field_name = "field_level{$i}_text";
      echo "<span class='feedback-$i' style='display:none;'>" . $quiz->{$field_name}[LANGUAGE_NONE][0]['safe_value'] . "</span>";
    }
    ?>
 </div>
  <!-- End of Feedback -->

  <!-- Quiz Show related feedback -->
  <div class='related-feedback'>
    <div><?php
    // we will write down the feedback for all the levels here - display none
    // will show the one in js
    for ($i = 1; $i <= 4; $i++) {
      $field_name = "field_rl_level{$i}_text";
      echo  "<li class='feedback-liststyle rl-feedback-$i' style='display:none;'>" . $quiz->{$field_name}[LANGUAGE_NONE][0]['safe_value'] . "</li>";
    }
?></div></div>
  <!-- End Show related feedback -->
  
  <!-- Show related links -->
  <div class="related-quiz">
    <ul>
<?php
$i = 0;
foreach ($quiz->field_related_links[LANGUAGE_NONE] AS $link) {
  #echo '<pre>'; print_r($related_quiz);
  $t = $link['title'];
  $l = $link['url'];
  echo '<li class="related-content">' . l($t, $l) . '</li>';
}
?>
    </ul>
  </div>
  </div>
  <!-- End Show related links -->
</div>


</div>
<!-- Quiz Final Slide Get Results End -->
