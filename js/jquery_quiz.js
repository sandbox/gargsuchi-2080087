function showNext(i, j) {
// used to start quiz and move to next questions
  jQuery(".question-" + j).hide();
    jQuery(".question-" + i).toggle('slide', {
              direction: 'right'
          }, 900);
  if (i == 1) {
    // hide introscreen
    jQuery(".intro").hide();
    // show scored and question counter
    jQuery(".qnumFull").show();
  }
  // update the question counter
  var qnum = jQuery(".qnum").html();
  qnum = parseInt(qnum) + 1;
  jQuery(".qnum").html(qnum);
  // hide the next question submit
  jQuery(".quiz-trigger-" + i).hide();
}


// used to show the quiz results
function showScore(i, j) {
  // hide scored and question counter
  jQuery(".qnumFull").hide();
  // hide last question
  jQuery(".question-" + j).hide();
  // show related quiz and score with legend
  jQuery(".get-results").toggle('slide', {
              direction: 'right'
          }, 1000);

  // update scored in quiz results
  var score = jQuery(".score").html();
  var score_percent1 = score / total_num_questions * 100;
  var score_percent = Math.round(score_percent1);

  // find the level now based on the percent
  if (score_percent <= 25) {
    var level = 1;
  }
  else if (score_percent <= 50) {
    var level = 2;
  }
  else if (score_percent <= 75) {
    var level = 3;
  }
  else {
    var level = 4;
  }
  
  // show the feedback/ related text and image based on level
  jQuery(".feedback-image-" + level).show();
  jQuery(".feedback-" + level).show();
  jQuery(".rl-feedback-" + level).show();
  
  jQuery("#scored").html(score);
  // update the score on fb social share links
  var tmp = jQuery('.fb-share a').attr('href');
  var tmp2 = tmp.replace(/SCORE/, score);
  jQuery('.fb-share a').attr('href', tmp2);
  
  // update the score on tw social share links
  var nid = jQuery("#nid").html();
  var countQ = jQuery("#ques-count").html();
  var quiztitle = jQuery("#quiztitle").html();
  UpdateTweetButton(score, nid, countQ, quiztitle);
  
}

// update the twitter button
function UpdateTweetButton(score, nid, countQ, quiztitle)
{
  var twitter_share_url = "http://twitter.com/intent/tweet?text=I scored a " + score + " out of " + countQ + " on " + quiztitle + " " + base_url + "/" + current_canonical_url + ".";
  jQuery('#tweet-url').attr('href', twitter_share_url);
}

// update the twitter button
function UpdateTweetButton1(score, nid, countQ, quiztitle)
{
  var elem3 = jQuery(document.createElement("a"));
  elem3.attr("class", "twitter-share-button");
  elem3.attr("href","https://twitter.com/share");
  elem3.attr("data-url",Drupal.settings.baseurl + "/node/" + nid);
  elem3.attr("data-counturl",Drupal.settings.baseurl + "/node/" + nid);
  elem3.attr("data-count", "vertical"); // none
  //elem3.attr("data-via", "");
  elem3.attr("data-text", "I scored a " + score + " out of " + countQ + " on " + quiztitle + ".");
  elem3.attr("data-lang","en");
  jQuery("#tweet").empty().append(elem3);

  return jQuery.getScript("http://platform.twitter.com/widgets.js",function(){
  twttr.widgets.load();
  });
}

// used to check the question option clicked
function alternativClicked(thisId, correctId, qi) {
  if (thisId == correctId) {
    var score = jQuery(".score").html();
    score = parseInt(score) + 1;
    jQuery(".score").html(score);
    jQuery("#" + qi + "-option-"+thisId).addClass("right-ans");
  }
  else {
    jQuery("#" + qi + "-option-"+correctId).addClass("right-ans");
    jQuery("#" + qi + "-option-"+thisId).addClass("wrong-ans");
  }
  // show the next question link
  jQuery(".quiz-trigger-" + qi).show();
  // remove the onlick attribute from alternative
  jQuery(".question-" + qi + " .alternative").removeAttr("onclick");
  // show the anwer explination
  jQuery(".explaination-" + qi).show();
  // remove the hover from alternative once clicked
  jQuery("#question-options-" + qi + " li").parent().find('li').removeClass("alternative");
}
