<?php
/**
 * @file
 * jquery_quiz.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jquery_quiz_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function jquery_quiz_image_default_styles() {
  $styles = array();

  // Exported image style: scale_230_120.
  $styles['scale_230_120'] = array(
    'name' => 'scale_230_120',
    'effects' => array(
      12 => array(
        'label' => 'Scale',
        'help' => 'Scaling will maintain the aspect-ratio of the original image. If only a single dimension is specified, the other dimension will be calculated.',
        'effect callback' => 'image_scale_effect',
        'dimensions callback' => 'image_scale_dimensions',
        'form callback' => 'image_scale_form',
        'summary theme' => 'image_scale_summary',
        'module' => 'image',
        'name' => 'image_scale',
        'data' => array(
          'width' => 230,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
      13 => array(
        'label' => 'Crop',
        'help' => 'Cropping will remove portions of an image to make it the specified dimensions.',
        'effect callback' => 'image_crop_effect',
        'dimensions callback' => 'image_resize_dimensions',
        'form callback' => 'image_crop_form',
        'summary theme' => 'image_crop_summary',
        'module' => 'image',
        'name' => 'image_crop',
        'data' => array(
          'width' => 230,
          'height' => 120,
          'anchor' => 'left-top',
        ),
        'weight' => 2,
      ),
    ),
    'label' => 'scale_230_120',
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function jquery_quiz_node_info() {
  $items = array(
    'quiz' => array(
      'name' => t('Quiz'),
      'base' => 'node_content',
      'description' => t('Custom quiz content type for ajax quiz.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
