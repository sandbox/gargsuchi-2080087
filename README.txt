-- SUMMARY --

The Feature when enabled creates a content type Quiz - using which users
can create simple one page quizzes which operate using jquery.

-- REQUIREMENTS --

ctools
entity
features
field_collection
field_collection_fieldset
image
link
list
media
multiselect
number
page_manager
panels
strongarm
text

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

None.

-- HOW TO USE --
 * Create a new node of the type Quiz.
 * Add in the question, option answers etc. 
 * Automatically the panel for the Quiz Content type will be enabled. 
   If it is not enabled, please enable it.
 * The node view will now be a panel page with an embedded block which has the 
   embedded jquery image.

Images can be overridden in the images folder (TBD - variabilize it)

Thats it! - Done.
